package com.linkdev.newsfeedtask.view.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.linkdev.newsfeedtask.R
import com.linkdev.newsfeedtask.base.ViewBindingActivity
import com.linkdev.newsfeedtask.databinding.ActivityDetailsBinding
import com.linkdev.newsfeedtask.model.NewsModel
import com.linkdev.newsfeedtask.utils.DateUtil

class DetailsActivity : ViewBindingActivity() {

    private lateinit var binding: ActivityDetailsBinding
    private lateinit var article: NewsModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        article = intent.getSerializableExtra("model") as NewsModel

        binding.apply {
            tvItemFeedNewsTitle.text = article.title
            tvItemFeedNewsAuthor.text = article.author
            tvItemFeedNewsDesc.text = article.description
            tvItemFeedNewsDate.text = DateUtil.getFormattedDate(article.publishedAt)

            Glide.with(this@DetailsActivity)
                .load(article.urlToImage)
                .placeholder(R.drawable.placeholder)
                .into(ivItemFeedNewsImage)
        }

        binding.btnRedirect.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW)
                    .setData(Uri.parse(article.url))
            )
        }
    }

    override fun getLayoutResource(): ConstraintLayout {
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home)
            finish()

        return true
    }
}