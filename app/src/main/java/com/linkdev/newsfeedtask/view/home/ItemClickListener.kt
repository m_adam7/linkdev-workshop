package com.linkdev.newsfeedtask.view.home

import com.linkdev.newsfeedtask.model.NewsModel

interface ItemClickListener {
    fun onItemClick(model: NewsModel)
}