package com.linkdev.newsfeedtask.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.linkdev.newsfeedtask.R
import com.linkdev.newsfeedtask.databinding.ItemArticleBinding
import com.linkdev.newsfeedtask.model.NewsModel
import com.linkdev.newsfeedtask.utils.DateUtil
import com.linkdev.newsfeedtask.view.home.ItemClickListener

class NewsFeedAdapter(
    private val context: Context,
    private val articles: ArrayList<NewsModel>,
    private val listener: ItemClickListener
) : RecyclerView.Adapter<NewsFeedAdapter.NewsViewHolder>() {

    class NewsViewHolder(val binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            ItemArticleBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val model = articles[position]
        holder.binding.apply {
            tvItemFeedNewsTitle.text = model.title
            tvItemFeedNewsAuthor.text = model.author
            tvItemFeedNewsDate.text = DateUtil.getFormattedDate(model.publishedAt)

            Glide.with(context)
                .load(model.urlToImage)
                .placeholder(R.drawable.placeholder)
                .into(ivItemFeedNewsImage)
        }

        holder.itemView.setOnClickListener {
            listener.onItemClick(model)
        }
    }

    override fun getItemCount(): Int = articles.size
}