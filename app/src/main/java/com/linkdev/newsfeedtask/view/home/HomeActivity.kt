package com.linkdev.newsfeedtask.view.home

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.navigation.NavigationView
import com.linkdev.newsfeedtask.R
import com.linkdev.newsfeedtask.base.ViewBindingActivity
import com.linkdev.newsfeedtask.databinding.ActivityHomeBinding
import com.linkdev.newsfeedtask.model.NewsModel
import com.linkdev.newsfeedtask.view.adapter.NewsFeedAdapter
import com.linkdev.newsfeedtask.view.details.DetailsActivity
import com.linkdev.newsfeedtask.viewmodel.NewsViewModel
import java.util.*


class HomeActivity : ViewBindingActivity(), NavigationView.OnNavigationItemSelectedListener,
    ItemClickListener {

    private lateinit var binding: ActivityHomeBinding
    private lateinit var viewmodel: NewsViewModel
    private lateinit var adapter: NewsFeedAdapter
    private val list = ArrayList<NewsModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(binding.appBar.toolbar)

        init()

    }

    private fun init() {

        binding.navView.setNavigationItemSelectedListener(this)

        viewmodel = ViewModelProvider(this).get(NewsViewModel::class.java)
        viewmodel.getNewsData()

        adapter = NewsFeedAdapter(this, list, this)
        binding.appBar.content.rvArticles.adapter = adapter

        viewmodel.newsData.observe(this, {
            hideProgress()
            list.addAll(it)
            adapter.notifyDataSetChanged()
        })

        viewmodel.errorData.observe(this, {
            hideProgress()
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        val actionBarDrawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this,
            binding.drawerLayout, binding.appBar.toolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close
        ) {
            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
            }
        }

        binding.drawerLayout.addDrawerListener(actionBarDrawerToggle)

        actionBarDrawerToggle.syncState()
    }

    private fun hideProgress() {
        binding.appBar.content.progressBar.visibility = View.GONE
    }

    override fun getLayoutResource(): ConstraintLayout {
        binding = ActivityHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        Toast.makeText(this, item.title, Toast.LENGTH_SHORT).show()
        return true
    }

    override fun onItemClick(model: NewsModel) {
        startActivity(
            Intent(this, DetailsActivity::class.java)
                .putExtra("model", model)
        )
    }

}