package com.linkdev.newsfeedtask.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object DateUtil {

    fun getFormattedDate(date: String?): String? {
        val old_format = "yyyy-MM-dd'T'hh:mm:ss'Z'" //2020-03-18T15:01:57Z
        val new_format = "MMM d, yyyy"
        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat(old_format)

        val dt: Date?
        try {
            dt = sdf.parse(date!!)
            sdf.applyPattern(new_format)
            return sdf.format(dt!!)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

}