package com.linkdev.newsfeedtask.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.linkdev.newsfeedtask.model.NewsModel
import com.linkdev.newsfeedtask.repository.NewsCallBack
import com.linkdev.newsfeedtask.repository.NewsRepo

class NewsViewModel : ViewModel() {

    val newsRepo = NewsRepo()
    val newsData = MutableLiveData<List<NewsModel>>()
    val errorData = MutableLiveData<String>()

    fun getNewsData() {

        newsRepo.getRemoteNews(object : NewsCallBack {
            override fun onSuccess(data: List<NewsModel>) {
                newsData.value = data
            }

            override fun onFailed(error: String) {
                errorData.value = error
            }

        })

    }

}