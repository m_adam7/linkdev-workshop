package com.linkdev.newsfeedtask.model

data class ResponseModel(
    val status: String?,
    val source: String?,
    val sortBy: String?,
    val articles: MutableList<NewsModel>
)
