package com.linkdev.newsfeedtask.model

import java.io.Serializable

data class NewsModel(
    val author: String?,
    val title: String?,
    val description: String?,
    val url: String?,
    val urlToImage: String?,
    val publishedAt: String?
) : Serializable
