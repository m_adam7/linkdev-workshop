package com.linkdev.newsfeedtask.repository

import com.linkdev.newsfeedtask.model.NewsModel

interface NewsCallBack {

    fun onSuccess(data: List<NewsModel>)
    fun onFailed(error: String)

}