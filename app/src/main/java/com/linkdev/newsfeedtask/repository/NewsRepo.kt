package com.linkdev.newsfeedtask.repository

import android.util.Log
import androidx.annotation.NonNull
import com.linkdev.newsfeedtask.model.NewsModel
import com.linkdev.newsfeedtask.model.ResponseModel
import com.linkdev.newsfeedtask.network.ApiClient
import com.linkdev.newsfeedtask.network.ApiService
import com.linkdev.newsfeedtask.utils.AppConstants
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*


class NewsRepo {

    private val list = ArrayList<NewsModel>()

    fun getRemoteNews(callBack: NewsCallBack) {

        val apiInterface: ApiService = ApiClient.getClient().create(ApiService::class.java)

        val ob1: Observable<ResponseModel> =
            apiInterface.getNewsData(AppConstants.SOURCE1, AppConstants.API_KEY)
        val ob2: Observable<ResponseModel> =
            apiInterface.getNewsData(AppConstants.SOURCE2, AppConstants.API_KEY)

        Observable.zip(
            ob1.subscribeOn(Schedulers.newThread()),
            ob2.subscribeOn(Schedulers.newThread()), mergeStringLists()
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<List<NewsModel?>?> {
                override fun onSubscribe(@NonNull d: Disposable?) {}
                override fun onNext(@NonNull list: List<NewsModel?>?) {
                    callBack.onSuccess(list as List<NewsModel>)
                }

                override fun onError(@NonNull e: Throwable) {
                    Log.d("error", e.message!!)
                    callBack.onFailed(e.message!!)
                }

                override fun onComplete() {}
            })

    }


    private fun mergeStringLists(): BiFunction<ResponseModel, ResponseModel, List<NewsModel>> {
        return BiFunction { response1: ResponseModel, response2: ResponseModel ->
            list.addAll(response1.articles)
            list.addAll(response2.articles)
            list
        }
    }

}