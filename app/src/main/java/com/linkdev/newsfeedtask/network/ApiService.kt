package com.linkdev.newsfeedtask.network

import com.linkdev.newsfeedtask.model.ResponseModel
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {

    @Headers("Accept: application/json")
    @GET("articles")
    fun getNewsData(
        @Query("source") source: String?,
        @Query("apiKey") key: String?
    ): Observable<ResponseModel>
}