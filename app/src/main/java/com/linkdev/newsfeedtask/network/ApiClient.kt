package com.linkdev.newsfeedtask.network

import com.google.gson.GsonBuilder
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ApiClient {

    private val BASE_URL = "https://newsapi.org/v1/"

    private val gson = GsonBuilder()
        .setLenient()
        .create()

    private lateinit var retrofit: Retrofit
    private val okHttpClient =
        OkHttpClient().newBuilder().addInterceptor { chain: Interceptor.Chain ->
            val originalRequest: Request = chain.request()
            val builder: Request.Builder = originalRequest.newBuilder()
            val newRequest: Request = builder.build()
            chain.proceed(newRequest)
        }.build()

    fun getClient(): Retrofit {
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
        return retrofit
    }
}